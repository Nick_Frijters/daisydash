Daisy Dash

--- SETUP RASPBERRY PI ---

1.
sudo apt-get update && sudo apt-get upgrade -y

2.
sudo apt-get install chromium-browser x11-xserver-utils unclutter

3.
sudo raspi-config
> enable ssh
> change admin password to 'dashboard'

4.
sudo chmod -R pi:pi ~/Documents/daisydash/

5.
sudo nano ~/.config/lxsession/LXDE-pi/autostart

6.
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
#@xscreensaver -no-splash
@point-rpi
@xset s off
@xset -dpms
@xset s noblank
@sed -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium-browser/Default/Preferences
@/home/pi/Documents/daisydash/src/system/keepalive.sh
@chromium-browser --noerrdialogs --disable-infobars --incognito --kiosk http://localhost:3000
@unclutter -idle 0.1 -root

7.
> CTRL + O
> ENTER
> CTRL + X

8.
Install chrome extension:
https://chrome.google.com/webstore/detail/autorefresh-on-error/cacnfebiodkggmdjhecdkendmeimjioa?hl=en

9.
sudo reboot

--- SETUP CONFIG ---

1.
Place images in /src/frontend/img

2.
Place config.json in /config

