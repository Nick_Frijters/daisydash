'use strict'

const DAY_SHORT = 'ddd DD MMM';
const DAY = 'dddd DD MMMM';
const TIME = 'HH:mm';
const DATE = 'YYYYMMDD';
const DATETIME = 'HH:mm (ddd)';
const COMPARISON = 'YYYYMMDDHHmm';

var socket = io.connect('http://localhost:3000/');

$.getScript("lib/layout.js");
$.getScript("lib/library.js");

socket.on('refresh', function(data) {
  //Is possibly undefined on first boot
  if (typeof printLog !== 'undefined') {
    printLog('Refresh command received');
  }
  location.reload(true);
});
