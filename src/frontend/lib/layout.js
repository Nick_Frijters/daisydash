'use strict'

var COLOR = {};

var wrapper = $(".wrapper");
var night = $(".nightScreen");

//Images
wrapper.append("<div class=''");

//Assistant
wrapper.append("<div class='module' id='assistant'><div id='assistantmessages'></div></div>");
$('#assistant').css("height", "calc(100% - 10px)");
$('#assistant').css("width", "calc(100% - 10px)");
$('#assistant').css("display", "table");
$('#assistantmessages').css("text-align", "center");
$('#assistantmessages').css("vertical-align", "bottom");
$('#assistantmessages').css("display", "table-cell");

//Weather
wrapper.append("<div class='module' id='weather'><div id='weathername'></div><div id='weatherdescription'></div><div id='weathertemp'></div></div>");
$('#weathername').css("margin-top", "80px");
$('#weathername').css("font-size", "30px");
$('#weathertemp').css("margin-top", "60px");
$('#weathertemp').css("font-size", "42px");
$('#weathertemp').css("font-weight", "bold");
$('#weatherdescription').css("margin-top", "15px");
$('#weatherdescription').css("font-size", "14px");
$('#weatherdescription').css("font-style", "italic");
$('#weatherwind').css("margin-top", "5px");
$('#weatherwind').css("font-size", "14px");

//Tasks
wrapper.append("<div class='module' id='tasks'></div>");
$('#tasks').css("font-size", "20px");
$('#tasks').css("text-align", "center");

//Datetime
wrapper.append("<div class='module' id='datetime'><div id='datetimecontent'><div id='date'></div><div id='time'></div></div></div>");
$('#datetime').css("height", "calc(100% - 10px)");
$('#datetime').css("width", "calc(100% - 10px)");
$('#datetime').css("font-weight", "bold");
$('#datetime').css("display", "table");
$('#datetimecontent').css("vertical-align", "middle");
$('#datetimecontent').css("display", "table-cell");
$('#date').css("font-size", "30px");
$('#date').css("margin-bottom", "30px");
$('#time').css("font-size", "60px");
night.append("<div id='nighttime'></div><div id='nighttemp'></div>");
$('#nighttime').css("font-size", "120px");
$('#nighttime').css("color", "#9E9E9E");
$('#nighttemp').css("font-size", "30px");
$('#nighttemp').css("color", "#9E9E9E");

//Counter
wrapper.append("<div class='module' id='counter'></div>");
$('#counter').css("padding-top", "140px");
$('#counter').css("font-size", "30px");

//System
wrapper.append("<div class='module' id='system'><div id='systemcontent'></div></div>");
$('#system').css("height", "calc(100% - 10px)");
$('#system').css("width", "calc(100% - 10px)");
$('#system').css("font-size", "14px");
$('#system').css("display", "table");
$('#systemcontent').css("text-align", "center");
$('#systemcontent').css("vertical-align", "middle");
$('#systemcontent').css("display", "table-cell");

//Calendar
wrapper.append("<div class='module' id='calendar'></div>");

//Calendar
wrapper.append("<div class='module' id='home'><canvas id='wifiStrength'></canvas></div>");
$('#home').css("padding-top", "30px");
$('#wifiStrength').css("display", "inline");

socket.on('update-layout', function(data) {
  setColor(data.color);
  setLayoutForImages(data.images);
  setLayoutForModule("assistant", data.assistant);
  setLayoutForModule("weather", data.weather);
  setLayoutForModule("tasks", data.tasks);
  setLayoutForModule("datetime", data.datetime);
  setLayoutForModule("counter", data.counter);
  setLayoutForModule("system", data.system);
  setLayoutForModule("calendar", data.calendar);
  setLayoutForModule("home", data.home);

  $('.loadingScreen').remove();
});

function setLayoutForImages(imageData) {
  if (imageData == null) {
    return;
  }
  var nrOfImages = imageData.length;
  for(var i = 0; i < nrOfImages; i++) {
    wrapper.append("<div class='module' id='image" + i + "'></div>");
    var div = $("#image" + i);
    div.css("grid-column", imageData[i].column);
    div.css("grid-row", imageData[i].row);
  }
}

function setLayoutForModule(name, module) {
  if (module == null) {
    $('#' + name).remove();
    return;
  }
  var div = $("#" + module.id);
  div.css("background-color", COLOR.color_50);
  div.css("grid-column", module.column);
  div.css("grid-row", module.row);
}

function setColor(color) {
  if (color === "RED") {
    COLOR.color_50 = "#FFEBEE";
    COLOR.color_100 = "#FFCDD2";
    COLOR.color_200 = "#EF9A9A";
    COLOR.color_300 = "#E57373";
    COLOR.color_400 = "#EF5350";
    COLOR.color_500 = "#F44336";
    COLOR.color_600 = "#E53935";
    COLOR.color_700 = "#D32F2F";
    COLOR.color_800 = "#C62828";
    COLOR.color_900 = "#B71C1C";
  } else if (color === "PINK") {
    COLOR.color_50 = "#FCE4EC";
    COLOR.color_100 = "#F8BBD0";
    COLOR.color_200 = "#F48FB1";
    COLOR.color_300 = "#F06292";
    COLOR.color_400 = "#EC407A";
    COLOR.color_500 = "#E91E63";
    COLOR.color_600 = "#D81B60";
    COLOR.color_700 = "#C2185B";
    COLOR.color_800 = "#AD1457";
    COLOR.color_900 = "#880E4F";
  } else if (color === "PURPLE") {
    COLOR.color_50 = "#F3E5F5";
    COLOR.color_100 = "#E1BEE7";
    COLOR.color_200 = "#CE93D8";
    COLOR.color_300 = "#BA68C8";
    COLOR.color_400 = "#AB47BC";
    COLOR.color_500 = "#9C27B0";
    COLOR.color_600 = "#8E24AA";
    COLOR.color_700 = "#7B1FA2";
    COLOR.color_800 = "#6A1B9A";
    COLOR.color_900 = "#4A148C";
  } else if (color === "DEEP_PURPLE") {
    COLOR.color_50 = "#EDE7F6";
    COLOR.color_100 = "#D1C4E9";
    COLOR.color_200 = "#B39DDB";
    COLOR.color_300 = "#9575CD";
    COLOR.color_400 = "#7E57C2";
    COLOR.color_500 = "#673AB7";
    COLOR.color_600 = "#5E35B1";
    COLOR.color_700 = "#512DA8";
    COLOR.color_800 = "#4527A0";
    COLOR.color_900 = "#311B92";
  } else if (color === "INDIGO") {
    COLOR.color_50 = "#E8EAF6";
    COLOR.color_100 = "#C5CAE9";
    COLOR.color_200 = "#9FA8DA";
    COLOR.color_300 = "#7986CB";
    COLOR.color_400 = "#5C6BC0";
    COLOR.color_500 = "#3F51B5";
    COLOR.color_600 = "#3949AB";
    COLOR.color_700 = "#303F9F";
    COLOR.color_800 = "#283593";
    COLOR.color_900 = "#1A237E";
  } else if (color === "BLUE") {
    COLOR.color_50 = "#E3F2FD";
    COLOR.color_100 = "#BBDEFB";
    COLOR.color_200 = "#90CAF9";
    COLOR.color_300 = "#64B5F6";
    COLOR.color_400 = "#42A5F5";
    COLOR.color_500 = "#2196F3";
    COLOR.color_600 = "#1E88E5";
    COLOR.color_700 = "#1976D2";
    COLOR.color_800 = "#1565C0";
    COLOR.color_900 = "#0D47A1";
  } else if (color === "LIGHT_BLUE") {
    COLOR.color_50 = "#E1F5FE";
    COLOR.color_100 = "#B3E5FC";
    COLOR.color_200 = "#81D4FA";
    COLOR.color_300 = "#4FC3F7";
    COLOR.color_400 = "#29B6F6";
    COLOR.color_500 = "#03A9F4";
    COLOR.color_600 = "#039BE5";
    COLOR.color_700 = "#0288D1";
    COLOR.color_800 = "#0277BD";
    COLOR.color_900 = "#01579B";
  } else if (color === "CYAN") {
    COLOR.color_50 = "#E0F7FA";
    COLOR.color_100 = "#B2EBF2";
    COLOR.color_200 = "#80DEEA";
    COLOR.color_300 = "#4DD0E1";
    COLOR.color_400 = "#26C6DA";
    COLOR.color_500 = "#00BCD4";
    COLOR.color_600 = "#00ACC1";
    COLOR.color_700 = "#0097A7";
    COLOR.color_800 = "#00838F";
    COLOR.color_900 = "#006064";
  } else if (color === "TEAL") {
    COLOR.color_50 = "#E0F2F1";
    COLOR.color_100 = "#B2DFDB";
    COLOR.color_200 = "#80CBC4";
    COLOR.color_300 = "#4DB6AC";
    COLOR.color_400 = "#26A69A";
    COLOR.color_500 = "#009688";
    COLOR.color_600 = "#00897B";
    COLOR.color_700 = "#00796B";
    COLOR.color_800 = "#00695C";
    COLOR.color_900 = "#004D40";
  } else if (color === "GREEN") {
    COLOR.color_50 = "#E8F5E9";
    COLOR.color_100 = "#C8E6C9";
    COLOR.color_200 = "#A5D6A7";
    COLOR.color_300 = "#81C784";
    COLOR.color_400 = "#66BB6A";
    COLOR.color_500 = "#4CAF50";
    COLOR.color_600 = "#43A047";
    COLOR.color_700 = "#388E3C";
    COLOR.color_800 = "#2E7D32";
    COLOR.color_900 = "#1B5E20";
  } else if (color === "LIGHT_GREEN") {
    COLOR.color_50 = "#F1F8E9";
    COLOR.color_100 = "#DCEDC8";
    COLOR.color_200 = "#C5E1A5";
    COLOR.color_300 = "#AED581";
    COLOR.color_400 = "#9CCC65";
    COLOR.color_500 = "#8BC34A";
    COLOR.color_600 = "#7CB342";
    COLOR.color_700 = "#689F38";
    COLOR.color_800 = "#558B2F";
    COLOR.color_900 = "#33691E";
  } else if (color === "LIME") {
    COLOR.color_50 = "#F9FBE7";
    COLOR.color_100 = "#F0F4C3";
    COLOR.color_200 = "#E6EE9C";
    COLOR.color_300 = "#DCE775";
    COLOR.color_400 = "#D4E157";
    COLOR.color_500 = "#CDDC39";
    COLOR.color_600 = "#C0CA33";
    COLOR.color_700 = "#AFB42B";
    COLOR.color_800 = "#9E9D24";
    COLOR.color_900 = "#827717";
  } else if (color === "YELLOW") {
    COLOR.color_50 = "#FFFDE7";
    COLOR.color_100 = "#FFF9C4";
    COLOR.color_200 = "#FFF59D";
    COLOR.color_300 = "#FFF176";
    COLOR.color_400 = "#FFEE58";
    COLOR.color_500 = "#FFEB3B";
    COLOR.color_600 = "#FDD835";
    COLOR.color_700 = "#FBC02D";
    COLOR.color_800 = "#F9A825";
    COLOR.color_900 = "#F57F17";
  } else if (color === "AMBER") {
    COLOR.color_50 = "#FFF8E1";
    COLOR.color_100 = "#FFECB3";
    COLOR.color_200 = "#FFE082";
    COLOR.color_300 = "#FFD54F";
    COLOR.color_400 = "#FFCA28";
    COLOR.color_500 = "#FFC107";
    COLOR.color_600 = "#FFB300";
    COLOR.color_700 = "#FFA000";
    COLOR.color_800 = "#FF8F00";
    COLOR.color_900 = "#FF6F00";
  } else if (color === "ORANGE") {
    COLOR.color_50 = "#FFF3E0";
    COLOR.color_100 = "#FFE0B2";
    COLOR.color_200 = "#FFCC80";
    COLOR.color_300 = "#FFB74D";
    COLOR.color_400 = "#FFA726";
    COLOR.color_500 = "#FF9800";
    COLOR.color_600 = "#FB8C00";
    COLOR.color_700 = "#F57C00";
    COLOR.color_800 = "#EF6C00";
    COLOR.color_900 = "#E65100";
  } else if (color === "DEEP_ORANGE") {
    COLOR.color_50 = "#FBE9E7";
    COLOR.color_100 = "#FFCCBC";
    COLOR.color_200 = "#FFAB91";
    COLOR.color_300 = "#FF8A65";
    COLOR.color_400 = "#FF7043";
    COLOR.color_500 = "#FF5722";
    COLOR.color_600 = "#F4511E";
    COLOR.color_700 = "#E64A19";
    COLOR.color_800 = "#D84315";
    COLOR.color_900 = "#BF360C";
  } else if (color === "BROWN") {
    COLOR.color_50 = "#EFEBE9";
    COLOR.color_100 = "#D7CCC8";
    COLOR.color_200 = "#BCAAA4";
    COLOR.color_300 = "#A1887F";
    COLOR.color_400 = "#8D6E63";
    COLOR.color_500 = "#795548";
    COLOR.color_600 = "#6D4C41";
    COLOR.color_700 = "#5D4037";
    COLOR.color_800 = "#4E342E";
    COLOR.color_900 = "#3E2723";
  } else if (color === "GREY") {
    COLOR.color_50 = "#FAFAFA";
    COLOR.color_100 = "#F5F5F5";
    COLOR.color_200 = "#EEEEEE";
    COLOR.color_300 = "#E0E0E0";
    COLOR.color_400 = "#BDBDBD";
    COLOR.color_500 = "#9E9E9E";
    COLOR.color_600 = "#757575";
    COLOR.color_700 = "#616161";
    COLOR.color_800 = "#424242";
    COLOR.color_900 = "#212121";
  } else  {
  // (color === "BLUE_GREY") is the default color
    COLOR.color_50 = "#ECEFF1";
    COLOR.color_100 = "#CFD8DC";
    COLOR.color_200 = "#B0BEC5";
    COLOR.color_300 = "#90A4AE";
    COLOR.color_400 = "#78909C";
    COLOR.color_500 = "#607D8B";
    COLOR.color_600 = "#546E7A";
    COLOR.color_700 = "#455A64";
    COLOR.color_800 = "#37474F";
    COLOR.color_900 = "#263238";
  }
}