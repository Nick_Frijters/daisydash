const modules = ['assistant', 'datetime', 'images', 'traffic', 'calendar', 'tasks', 'weather', 'system', 'counter', 'home', 'advertisements'];

var lang;
var modulesLoaded = 0;
var configRequested = false;

socket.on('update-lang', function(data) {
  printLog('Language received');
  lang = data;
  moment.locale(lang.LOCALE);
});

$.getScript("/lib/logging.js", function(){
  printLog('Window size: ' + window.innerWidth + 'x' + window.innerHeight);

  requestLanguage();
  loadModulesAfterRequestingLanguage();
});

function requestLanguage() {
  printLog('Requesting language...');
  socket.emit('request-lang', {});
}

function dynamicMessage(message, params) {
  for (var i = 0; i < params.length; i++) {
    message = message.replace('$$', params[i]);
  }
  return message;
}

function getModLocation(module) {
  return "/lib/mod/" + module + ".js";
}

function loadModulesAfterRequestingLanguage() {
  if (lang !== undefined) {
    for (var i = 0; i < modules.length; i++) {
      $.getScript(getModLocation(modules[i]), function(data, textStatus, jqxhr){
        // Code (jqxhr.status) can be anything defined here:
        // https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
        printLog('Loaded module: ' + textStatus + ' (code: ' + jqxhr.status + ')');

        modulesLoaded++;
      });
    }
    requestConfigAfterLoadingModules();
  } else {
    setTimeout( function() {
      loadModulesAfterRequestingLanguage();
    }, 50);
  }
}

function requestConfigAfterLoadingModules() {
  if (!configRequested) {
    if (modulesLoaded === modules.length) {
      socket.emit('request-config', {});
      configRequested = true;
    } else {
	  setTimeout( function() {
        requestConfigAfterLoadingModules();
      }, 50);
    }
  }
}