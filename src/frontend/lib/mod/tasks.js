'use strict'

socket.on('update-tasks', function(data) {
  showTasks(data);
});

function showTasks(tasks) {
  var table = $('<table class="tasks"></table>');
  var tableBody = $(document.createElement('tbody'));

  tasks.forEach(function (task) {
    appendItem(tableBody, task);
  });

  table.append(tableBody);

  $("#tasks").empty();
  $("#tasks").append(table);
}

function appendItem(tableBody, task) {
  var row = $('<tr></tr>');
  var dateText = '';
  if (task.due !== undefined && task.due !== null) {
    dateText = ' <font size="-2" color="gray">(' + moment(task.due).format(DAY_SHORT) + ')</font>';
  }
  row.append($('<td class="stretch-cell task" style="background: ' + task.color + '; border: 1px solid ' + COLOR.color_900 + '"></td>'));
  row.append($('<td class="stretch-cell"></td>').html(task.name + dateText));
  tableBody.append(row);
}
