'use strict';

const SPECIAL_THANKS = "Joyce Frijters, Dean Frijters";

var ipKnown = false;

socket.on('update-system', function(data) {
  showSystem(data);
  updateIp();
});

function showSystem(data) {
  $("#systemcontent").html(
    '</br></br>' +
    '<font size="+3">' + data.description + '</font></br>' +
    '</br>' +
    lang.version + ': ' + data.version + '</br>' +
    '</br></br></br></br></br></br>' +
    '<font size="-1">&copy 2017-' + moment().year() + ' Nick Frijters</br>' +
    'frijters.nick@gmail.com</br>' +
    'https://www.facebook.com/daisydashboard/</font></br>' +
    '<marquee scrollamount="3"><font size="-1"><i>' + dynamicMessage(lang.specialThanks, [SPECIAL_THANKS]) + '</i></font></marquee>' +
    '</br>'
  );
}

function updateIp() {
  function getIPs(callback){
    var ip_dups = {};

    var RTCPeerConnection = window.RTCPeerConnection
      || window.mozRTCPeerConnection
      || window.webkitRTCPeerConnection;

    if(!RTCPeerConnection){
      var win = iframe.contentWindow;
      RTCPeerConnection = win.RTCPeerConnection
        || win.mozRTCPeerConnection
        || win.webkitRTCPeerConnection;
    }

    var mediaConstraints = {
      optional: [{RtpDataChannels: true}]
    };

    var servers = {iceServers: [{urls: "stun:stun.services.mozilla.com"}]};
    var pc = new RTCPeerConnection(servers, mediaConstraints);

    function handleCandidate(candidate){
      var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
      var ip_addr = ip_regex.exec(candidate)[1];

      if(ip_dups[ip_addr] === undefined)
        callback(ip_addr);

      ip_dups[ip_addr] = true;
    }

    pc.onicecandidate = function(ice) {
      if(ice.candidate)
        handleCandidate(ice.candidate.candidate);
      };

    pc.createDataChannel("");
    pc.createOffer(function(result) {
      pc.setLocalDescription(result, function(){}, function(){});
    }, function(){});

    setTimeout(function() {
      var lines = pc.localDescription.sdp.split('\n');
      lines.forEach(function(line) {
        if(line.indexOf('a=candidate:') === 0)
          handleCandidate(line);
      });
    }, 1000);
  }

  getIPs(function(ip){
    if (!ipKnown) {
      assistantMessage('technical_status', 'IP: ' + ip);
      ipKnown = true;
    }
  });
}
