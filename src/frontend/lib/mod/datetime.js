loadHolidays(lang.LOCALE);
showDateTime();

var lastHoliday;

function showDateTime() {
  showDate();
  showTime();
  showHoliday();

  setTimeout( function() {
    showDateTime();
  }, 1000);
}

function showDate()
{
  var dayDate = $('#date');

  dayDate.html(
    '<span id="holiday"></span>' +
    moment().format('dddd DD MMMM')
  );
}

function showTime()
{
  var dayTime = $('#time');
  var nightTime = $('#nighttime');

  var time = moment();

  dayTime.html(time.format('HH') + '<span id="ch1">:</span>' + time.format('mm') + '<span id="ch2">:</span>' + time.format('ss'));
  //toggle hands
  dayTime.find('#ch1').fadeTo(800, 0);
  dayTime.find('#ch2').fadeTo(800, 0);

  nightTime.html(time.format('HH:mm'));

  if (moment().isBetween(moment('05:30:00', 'hh:mm:ss'), moment('22:00:00', 'hh:mm:ss')))
  {
    $(".nightScreen").hide();
  } else {
    $(".nightScreen").show();
  }
}

function showHoliday() {
  var holiday = moment().isHoliday();

  if (holiday) {
    $('#holiday').html('<font size="+1">' + holiday + '</font></br>');
    if (holiday !== lastHoliday) {
      lastHoliday = holiday;
      assistantMessage('holidays', dynamicMessage(lang.holidayToday, [holiday]));
    }
  }
}

function loadHolidays(locale) {
  if (locale === 'nl') {
    moment.modifyHolidays.add({
      "Nieuwjaarsdag": {
        date: '1/1'
      },
      "Drie Koningen": {
        date: '1/6'
      },
      "Valentijnsdag": {
        date: '2/14'
      },
      "Koningsdag": {
        date: '4/27'
      },
      "Dag van de Arbeid": {
        date: '5/1'
      },
      "Dodenherdenking": {
        date: '5/4'
      },
      "Bevrijdingsdag": {
        date: '5/5'
      },
      "Dierendag": {
        date: '10/4'
      },
      "Halloween": {
        date: '10/31'
      },
      "Allerheiligen": {
        date: '11/1'
      },
      "Sint Maarten": {
        date: '11/11'
      },
      "Sinterklaas": {
        date: '12/5'
      },
      "Eerste Kerstdag": {
        date: '12/25'
      },
      "Tweede Kerstdag": {
        date: '12/26'
      },
      "Oudjaarsdag": {
        date: '12/31'
      },

      //Dynamic days
      "Carnaval": {
        date: 'easter-51'
      },
      "Carnaval": {
        date: 'easter-50'
      },
      "Carnaval": {
        date: 'easter-49'
      },
      "Carnaval": {
        date: 'easter-48'
      },
      "Carnaval": {
        date: 'easter-47'
      },
      "Aswoensdag": {
        date: 'easter-46'
      },
      "Goede Vrijdag": {
        date: 'easter-2'
      },
      "Eerste Paasdag": {
        date: 'easter'
      },
      "Tweede Paasdag": {
        date: 'easter+1'
      },
      "Hemelvaartsdag": {
        date: 'easter+39'
      },
      "Eerste Pinksterdag": {
        date: 'easter+49'
      },
      "Tweede Pinksterdag": {
        date: 'easter+50'
      },

      "Moederdag": {
        date: '5/(0,2)'
      },
      "Vaderdag": {
        date: '6/(0,3)'
      },
      "Prinsjesdag": {
        date: '9/(3,3)'
      }
    });
  }
}