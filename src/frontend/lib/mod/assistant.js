'use strict';

var assistantConfig = false;

assistantMessage('assistant', lang.assistantGreeting);
scrollMessages();

socket.on('update-assistant', function(config) {
  assistantConfig = config;
});

socket.on('assistant-message', function(messagObject) {
  assistantMessage(messagObject.type, messagObject.message);
});

function assistantMessage(type, message) {
  if (!allowMessage(type)) {
    return;
  }

  var messageDiv = $('<div class="message"></div>');

  var time = moment();
  messageDiv.html(message + ' <font size="-2" color="gray">(' + time.format('ddd HH:mm') + ')</font>');

  $("#assistantmessages").append(messageDiv);

  cleanupMessages();
}

function scrollMessages() {
  var newestMessage = $(".message:last");
  var marginBottom = newestMessage.css('margin-bottom');
  if (marginBottom) {
    var marginBottomInt = marginBottom.substring(0, marginBottom.length-2);
    newestMessage.css('margin-bottom', (Number(marginBottomInt) + 1) + 'px');

    cleanupMessages();
  }

  setTimeout( function() {
    scrollMessages();
  }, 60000);
}

function cleanupMessages() {
  var clean = false;
  while (!clean) {
    var oldestMessage = $(".message:first");
    if (oldestMessage.position() && oldestMessage.position().top < 20) {
      oldestMessage.remove();
    } else {
      clean = true;
    }
  }
}

function allowMessage(type) {
  switch(type) {
    case 'advertisement':
    case 'assistant':
    case 'update':
    case 'offer':
      return true;
    default:
      return assistantConfig[type] !== false
  }
}