'use strict'

var counterDescription, counterStart, counterEnd, start, end;
var prevDays = 999;
var prevMonths = 999;
var prevYears = 999;

socket.on('update-counter', function(data) {
  counterDescription = data.event;
  counterStart = data.start;
  counterEnd = data.end;
  showCounter();
});

function showCounter() {
  start = counterStart === "now" ? moment() : moment(counterStart);
  end = counterEnd === "now" ? moment() : moment(counterEnd);

  var years = end.diff(start, 'year');
  start.add(years, 'years');

  var months = end.diff(start, 'months');
  start.add(months, 'months');

  var days = end.diff(start, 'days');

  $("#counter").html(
    counterDescription + "</br></br>" +
    "<font size='+1'>" +
    years + " " + lang.years + "</br>" +
    months + " " + lang.months + "</br>" +
    days + " " + lang.days +
    "</font>"
  );

  if (prevDays !== 999 && prevMonths !== 999 && prevYears !== 999) {
    if (months > prevMonths || years > prevYears) {
      assistantMessage('counter', dynamicMessage(lang.counterMilestone, [counterDescription, years, months]));
    }
    if (days < prevDays) {
      assistantMessage('counter', dynamicMessage(lang.counterRemaining, [years, months, days, counterDescription]));
    }
  }

  prevDays = days;
  prevMonths = months;
  prevYears = years;

  setTimeout( function() {
    showCounter();
  }, 60000);
}
