'use strict'

var home, travelMode, currentTravelPlan;
var plannedEvents = [];
const FIVE_MINUTES = 300;
const HALF_HOUR = 1800;

socket.on('init-traffic', function(data) {
  home = data.home;
  travelMode = google.maps.DirectionsTravelMode.DRIVING;
});

function prepareToLeave(calendarItem, delay) {
  if (plannedEvents.includes(calendarItem.id)) {
    //Event is already planned
    return;
  }

  plannedEvents.push(calendarItem.id);

  if (moment(calendarItem.start.dateTime).subtract(HALF_HOUR, 'seconds') < moment()) {
    //Too late to plan message
    return;
  }

  if (calendarItem.location === undefined) {
    //This is fine, no location is set
    return;
  }

  var directionsService = new google.maps.DirectionsService();
  var request = {
    origin: home,
    destination: calendarItem.location,
    travelMode: travelMode
  };

  directionsService.route(request, function(response, status) {
    if (status === 'OK') {
      var travelTimeSec = response.routes[0].legs[0].duration.value;
      var arrivalInSec = moment(calendarItem.start.dateTime).subtract(Math.max(FIVE_MINUTES, travelTimeSec * 0.2), 'seconds').diff(moment(), 'seconds');
      var leaveInSec = (arrivalInSec - travelTimeSec);

      var leaveMoment = moment().add(leaveInSec, 'seconds').format(TIME);
      var arrivalMoment = moment().add(arrivalInSec, 'seconds').format(TIME);
      var appointmentMoment = moment(calendarItem.start.dateTime).format(TIME);

      if (delay) {
        var messageInMs = ((leaveInSec - HALF_HOUR) * 1000);
        var recalcMs = ((leaveInSec - FIVE_MINUTES) * 1000);

        if (messageInMs > 0) {
          setTimeout( function() {
            assistantMessage('traffic', dynamicMessage(lang.trafficDepartMessage, [leaveMoment, arrivalMoment, calendarItem.color, calendarItem.summary, appointmentMoment]));
          }, messageInMs);
        }

        if (recalcMs > 0) {
          setTimeout( function() {
            prepareToLeave(calendarItem, false);
          }, recalcMs);
        }
      } else {
        assistantMessage('traffic', dynamicMessage(lang.trafficDepartMessage, [leaveMoment, arrivalMoment, calendarItem.color, calendarItem.summary, appointmentMoment]));
      }
    } else if (status === 'NOT_FOUND') {
      //This is fine, it happens when a location is set like "At home" or "Somewhere" instead of an actual address
    } else {
      printLog("Unable to retrieve traffic information: " + status);
    }
  });
}