'use strict';

var calendarEvents = [];

socket.on('update-calendar', function(data) {
  data = [].concat.apply([], data);
  calendarEvents = formatCalendarData(data);
  showCalendar();
  for (var i = 0; i < Math.min(calendarEvents.length, 5); i++) {
    prepareToLeave(calendarEvents[i], true);
  }
});

socket.on('update-calendar-event', function(event) {
  var startDate = moment(event.start.dateTime).format(DAY);
  var startTime = moment(event.start.dateTime).format(TIME);
  assistantMessage('counter', dynamicMessage(lang.calendarEventUpdated, [event.summary, startDate, startTime]));
});

function formatCalendarData(data) {
  var newCalendarEvents = [];
  data.forEach(function (incomingCalendarEvent) {
    newCalendarEvents.push({
      id: incomingCalendarEvent.updated,
      color: decodeColor(incomingCalendarEvent),
      start: incomingCalendarEvent.start,
      end: incomingCalendarEvent.end,
      updated: incomingCalendarEvent.updated,
      summary: incomingCalendarEvent.summary,
      location: incomingCalendarEvent.location
    });
  });
  return newCalendarEvents;
}

function showCalendar() {
  var table = $('<table class="calendar"></table>');
  var tableBody = $(document.createElement('tbody'));
  var lastDate = moment().format(DATE);
  var rowCounter = 0;

  calendarEvents = filterCalendarEvents(calendarEvents);

  calendarEvents.forEach(function (calendarEvent) {
    var eventDate = moment(calendarEvent.start.date || calendarEvent.start.dateTime).format(DATE);

    while (lastDate <= eventDate) {
      var row = $('<tr class="section"></tr>');
      row.append($('<td class="stretch-cell" style="background: ' + COLOR.color_100 + '; border: 1px solid ' + COLOR.color_900 + '" colspan="2"></td>').html('<font color="' + COLOR.color_900 + '">' + moment(lastDate).format(DAY) + '</font>'));
      tableBody.append(row);
      rowCounter++;
      lastDate = moment(lastDate).add(1, 'days').format(DATE);
    }

    var newText = moment(calendarEvent.updated) > moment().subtract(1, 'day') ? ' (' + lang.changed + ')' : '';
    var row = $('<tr></tr>');
    if (calendarEvent.start.date !== undefined) {
      row.append($('<td class="crop-cell" style="background: ' + calendarEvent.color + '"></td>').text(lang.allDay));
    } else {
      var startDateTime = moment(calendarEvent.start.dateTime);
      var endDateTime = moment(calendarEvent.end.dateTime);
      if (endDateTime.format(DATE) > startDateTime.format(DATE)) {
        var endString = endDateTime.format(DATETIME);
      } else {
        var endString = endDateTime.format(TIME);
      }
      row.append($('<td class="crop-cell" style="background: ' + calendarEvent.color + '"></td>').text(startDateTime.format(TIME) + ' - ' + endString + newText));
    }
    row.append($('<td class="stretch-cell"></td>').text(calendarEvent.summary));
    tableBody.append(row);
    rowCounter++;
  });

  table.append(tableBody);

  $("#calendar").empty();
  $("#calendar").append(table);
}

function filterCalendarEvents(calendarEvents) {
  calendarEvents.sort(function(a,b) {
    var aDate = moment(a.start.date || a.start.dateTime);
    var bDate = moment(b.start.date || b.start.dateTime);
    return aDate - bDate;
  });

  calendarEvents = calendarEvents.filter(function (event) {
    var eventDate = moment(event.start.date || event.start.dateTime);
    return eventDate.isBefore(moment().add(1, 'week'));
  });

  return calendarEvents;
}

function decodeColor(calendarEvent) {
  switch(calendarEvent.colorId) {
    case '1': return '#3F51B5'; //Bosbes
    case '2': return '#33B679'; //Salie
    case '3': return '#8E24AA'; //Druif
    case '4': return '#E67C73'; //Flamingo
    case '5': return '#F6BF26'; //Zonnebloem
    case '6': return '#D50000'; //Tomaat
    case '7': return '#039BE5'; //Pauw
    case '8': return '#616161'; //Grafiet
    case '9': return '#7986CB'; //Lavendel
    case '10': return '#0B8043'; //Basilicum
    default: return '#A9A9A9';
  }
}