'use strict';

var networkChartContext, networkChart, networkChartConfig;
var networkName = 'Network';

$(document).ready(function() {
  networkChartContext = $("#wifiStrength").get(0).getContext("2d");
  networkChartConfig = {
    type: 'bar',
    data: {
      datasets: [
        {
          data: [50],
          backgroundColor: '#BDBDBD',
          borderColor: '#757575',
          borderWidth: 1
        },
        {
          data: [50],
          backgroundColor: '#FAFAFA',
          borderColor: '#757575',
          borderWidth: 1
        },
        {
          data: [0],
          borderColor: '#757575',
          borderWidth: 1
        }
      ]
    },
    options: {
      title: {
        display: true,
        fontSize: 20,
        fontColor: 'black',
        text: 'Network: 0%'
      },
      legend: {
        display: false
      },
      elements: {
        rectangle: {
          borderSkipped: 'top'
        }
      },
      scales: {
        xAxes: [{
          stacked: true,
          display: false,
          ticks: {
            beginAtZero: true
          }
        }],
        yAxes: [{
          stacked: true,
          display: false,
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  };
  networkChart = new Chart(networkChartContext, networkChartConfig);
});

socket.on('update-wifi-quality', function(quality) {
  networkChartConfig.options.title.text = networkName + ': ' + quality + '%';
  networkChartConfig.data.datasets[0].data[0] = quality;
  networkChartConfig.data.datasets[1].data[0] = (100 - quality);
  networkChart.update();
});

socket.on('update-wifi-ssid', function(ssid) {
  if (ssid.length > 0) {
    networkName = ssid;
  } else {
    networkName = 'Network';
  }
})