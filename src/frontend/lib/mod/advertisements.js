'use strict';

const ONE_DAY = 86400000;

var ads = [];

initAds();

socket.on('update-advertisements', function(data) {
  ads = data;
  initAds();
});

function initAds() {
  ads.forEach(advertise);
}

function advertise(ad) {
  var now = new Date();
  var timeDiff = new Date(now.getFullYear(), now.getMonth(), now.getDate(), ad.h, ad.m, ad.s, 0) - now;
  var waitTime = timeDiff >= 0 ? timeDiff : (timeDiff + ONE_DAY);

  setTimeout( function() {
    assistantMessage('advertisement', lang[ad.message]);
    if (!!ad.once) {
      advertise(ad);
    }
  }, waitTime);
}