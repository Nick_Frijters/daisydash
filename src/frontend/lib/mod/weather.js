'use strict';

var key, place;
var mode = "json";
var weatherTimeout = 5000;
var wThunderstorm = false;
var wDrizzle = false;
var wRain = false;
var wSnow = false;
var weatherWindSpeed;
var weatherTemperature;

socket.on('update-weather', function(data) {
  key = data.key;
  place = data.location;
});

updateWeatherStatus();

function updateWeatherStatus() {
  if (place !== undefined && key !== undefined) {
    weatherTimeout = 60000;
    $.getJSON('http://api.openweathermap.org/data/2.5/weather?units=metric&lang=' + lang.LOCALE + '&q=' + place + '&mode=' + mode + '&appid=' + key, function(result){
      var weatherCode = '';
      for (var i = 0; i < result.weather.length; i++) {
        //http://openweathermap.org/weather-conditions
        weatherCode += (result.weather[i].id + '').charAt(0);
      }
      if ((weatherCode.indexOf('2') > -1) !== wThunderstorm) {
        wThunderstorm = !wThunderstorm;
        assistantMessage('weather', wThunderstorm ? lang.weatherThunderstormStart : lang.weatherThunderstormStop);
      }
      if ((weatherCode.indexOf('6') > -1) !== wSnow) {
        wSnow = !wSnow;
        assistantMessage('weather', wSnow ? lang.weatherSnowStart : lang.weatherSnowStop);
      }

      var newTemperature = result.main.temp;
      if (newTemperature <= 0 && weatherTemperature > 0) {
        assistantMessage('weather', lang.weatherFreezingStart);
      }
      if (newTemperature > 0 && weatherTemperature <= 0) {
        assistantMessage('weather', lang.weatherFreezingStop);
      }
      weatherTemperature = newTemperature

      $("#weathername").html(result.name);
      $("#weatherdescription").html(result.weather[0].description + '</br>' + getWindSpeed(result.wind.speed));
      $("#weathertemp").html('<img src="http://openweathermap.org/img/w/' + result.weather[0].icon + '.png"/> ' + Math.round(result.main.temp) + '&deg;C');
      $("#nighttemp").html('<img src="http://openweathermap.org/img/w/' + result.weather[0].icon + '.png"/> ' + Math.round(result.main.temp) + '&deg;C');
    });
  }

  setTimeout( function() {
    updateWeatherStatus()
  }, weatherTimeout);
}

function getWindSpeed(speedms) {
  if(speedms < 0.3) {
    return lang.weatherWindCalm;
  } else if (speedms <= 1.5) {
    return lang.weatherWindLightAir;
  } else if (speedms <= 3.3) {
    return lang.weatherWindLightBreeze;
  } else if (speedms <= 5.5) {
    return lang.weatherWindGentleBreeze;
  } else if (speedms <= 7.9) {
    return lang.weatherWindModerateBreeze;
  } else if (speedms < 10.7) {
    return lang.weatherWindFreshBreeze;
  } else if (speedms <= 13.8) {
    return lang.weatherWindStrongBreeze;
  } else if (speedms <= 17.1) {
    return lang.weatherWindNearGale;
  } else if (speedms <= 20.7) {
    return lang.weatherWindGale;
  } else if (speedms <= 24.4) {
    return lang.weatherWindStrongGale;
  } else if (speedms <= 28.4) {
    return lang.weatherWindStorm;
  } else if (speedms <= 32.6) {
    return lang.weatherWindViolentStorm;
  } else {
    return lang.weatherWindHurricaneForce;
  }
}
