'use strict'

var index = 0;

socket.on('update-image', function(image) {
  if ($('#image' + index).length == 0) {
    index = 0;
  }
  var div = $('#image' + index++ + ':first');
  div.css("background-image", "url('data:image/" + image.type + ";base64," + image.buffer + "')");
  div.css("background-position", "center");
  div.css("background-repeat", "no-repeat");
  div.css("background-size", "cover");
});