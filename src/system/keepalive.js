var cp = require('child_process');
var fs = require('fs');
var download = require('download-git-repo');
var request = require('request');

const host = 'bitbucket.org';
const repo = 'Nick_Frijters/daisydash';
const branch = 'develop';
const packageJson = 'package.json';
const package = '/raw/' + branch + '/src/' + packageJson;
const rootFolder = 'Documents/daisydash';
const downloadFolder = rootFolder + '/download';
const targetFolder = rootFolder + '/app';
const targetFolderSrc = rootFolder + '/app/src';
const keepaliveTime = 1000;
const checkTime = 86400000;
const restartTime = 31536000000;
//One Second = 1000
//Hourly = 3600000
//Daily = 86400000
//Weekly = 604800000
//Monthly = 2592000000
//Yearly = 31536000000

const OFF = "OFF";
const ON = "ON";
const CHECKING_LOCAL_VERSION = "CHECKING LOCAL VERSION";
const CHECKED_LOCAL_VERSION = "CHECKED LOCAL VERSION";
const CHECKING_GIT_VERSION = "CHECKING GIT VERSION";
const CHECKED_GIT_VERSION = "CHECKED GIT VERSION";
const NEW_VERSION_FOUND = "NEW VERSION FOUND";
const DOWNLOADING = "DOWNLOADING";
const DOWNLOADED = "DOWNLOADED";
const INSTALLING_DASHBOARD = "INSTALLING DASHBOARD";
const INSTALLED_DASHBOARD = "INSTALLED DASHBOARD";
const INSTALLING_DEPENDENCIES = "INSTALLING DEPENDENCIES";
const INSTALLED_DEPENDENCIES = "INSTALLED DEPENDENCIES";
var state = OFF;

const NO_VERSION = '0.0.0';
var localVersion = NO_VERSION;
var gitVersion = NO_VERSION;

var server = false;
var updating = false;
var waitingCheck = checkTime;
var waitingRestart = 0;

keepalive();

function keepalive() {
  if (getState() === ON && waitingRestart >= restartTime) {
    waitingRestart = 0;
    stopServer();
    setState(OFF);
  }

  if (waitingCheck >= checkTime) {
    waitingCheck = 0;
    setState(CHECKING_LOCAL_VERSION);
    updateLocalVersion();
  }

  if (getState() === CHECKED_LOCAL_VERSION) {
    setState(CHECKING_GIT_VERSION);
    updateGitVersion();
  }

  if (getState() === CHECKED_GIT_VERSION) {
    if (isNewVersionAvailable()) {
      setState(NEW_VERSION_FOUND);
      stopServer();
      downloadUpdate();
    } else if (!server) {
      setState(OFF);
    } else {
      setState(ON);
    }
  }

  if (getState() === DOWNLOADED) {
    installDashboard();
  }

  if (getState() === INSTALLED_DASHBOARD) {
    installDependencies();
  }

  if (getState() === OFF || getState() === INSTALLED_DEPENDENCIES) {
    if (!server) {
      startServer();
    }
  }

  setTimeout( function() {
    waitingCheck += keepaliveTime;
    waitingRestart += keepaliveTime;
    keepalive();
  }, keepaliveTime);
}

function setState(newState) {
  state = newState;
}

function getState() {
  return state;
}

function updateLocalVersion() {
  if (!fs.existsSync(targetFolderSrc + '/' + packageJson)) {
    localVersion = NO_VERSION;
  } else {
    localVersion = JSON.parse(fs.readFileSync(targetFolderSrc + '/' + packageJson, 'utf8')).version;
  }
  setState(CHECKED_LOCAL_VERSION);
}

function updateGitVersion() {
  var options = {
    hostname: host,
    path: '/' + repo + package
  };

  request('https://' + host + '/' + repo + package, function(error, response, body) {
    if (!response || (response && response.statusCode !== 200)) {
      console.log('Keepalive: Problem retrieving version, https response status: ' + response.statusCode);
      gitVersion = NO_VERSION;
    } else {
      gitVersion = JSON.parse(response.body).version;
    }
    setState(CHECKED_GIT_VERSION);
  });
}

function isNewVersionAvailable() {
  var local = localVersion.split(".");
  var git = gitVersion.split(".");
  for (var i = 0; i < git.length; i++) {
    if(local[i] === undefined || parseInt(git[i]) > parseInt(local[i])) {
      console.log("Keepalive: Found a new remote version " + gitVersion + " (local version is " + localVersion + ")");
      return true;
    }
  }
  return false;
}

function downloadUpdate() {
  setState(DOWNLOADING);
  console.log('Keepalive: Cleaning download folder');
  deleteFolderRecursive(downloadFolder);

  console.log('Keepalive: Downloading new version');
  download('bitbucket:' + repo + '#' + branch, downloadFolder, { clone: false }, function (err) {
    if (err) {
      console.log("Keepalive: Error downloading new version, " + err);
      setState(OFF);
    } else {
      console.log("Keepalive: Repository download success");
      setState(DOWNLOADED);
    }
  });
}

function installDashboard() {
  setState(INSTALLING_DASHBOARD);
  console.log('Keepalive: Installing new version');
  console.log('Keepalive: Cleaning target folder');
  deleteFolderRecursive(targetFolder);
  console.log('Keepalive: Moving new version to target folder');
  fs.renameSync(downloadFolder, targetFolder);
  setState(INSTALLED_DASHBOARD);
}

function installDependencies() {
  setState(INSTALLING_DEPENDENCIES);
  console.log("Keepalive: Installing dependencies 0%");
  cp.exec('npm --prefix ./' + targetFolderSrc + ' install ./' + targetFolderSrc, function(error, stdout, stderr) {
    if(stdout) {
      console.log('Keepalive: npm install STDOUT: ' + stdout);
    }
    if(stderr) {
      console.log('Keepalive: npm install STDERR: ' + stderr);
    }
    if(error) {
      console.log('Keepalive: npm install ERROR: ' + error.code);
    } else {
      console.log("Keepalive: Installing dependencies 100%");
      console.log("Keepalive: Updating dependencies 0%");
      cp.exec('npm --prefix ./' + targetFolder + ' update ./' + targetFolder, function(error, stdout, stderr) {
        if(stdout) {
          console.log('Keepalive: npm update STDOUT: ' + stdout);
        }
        if(stdout) {
          console.log('Keepalive: npm update STDERR: ' + stdout);
        }
        if(error) {
          console.log('Keepalive: npm upate ERROR: ' + error.code);
        } else {
          console.log("Keepalive: Updating dependencies 100%");
        }
        setState(INSTALLED_DEPENDENCIES);
      });
    }
  });
}

function stopServer() {
  console.log("Keepalive: Stopping server (current state: " + state + ")");
  if(!server) {
    console.log("Keepalive: Can't stop server because it's not running");
  } else {
    server.kill('SIGHUP');
  }
}

function startServer() {
  console.log("Keepalive: Starting server");
  server = cp.fork(targetFolder + '/src/backend/dashboard.js');
  setState(ON);

  server.on('close', (code) => {
    console.log('Keepalive: Server close (code): ' + code);
    //Don't set state to OFF, because it could be updating
    server = false;
  });

  server.on('error', (err) => {
    console.log('Keepalive: Server error: ' + err);
    //Don't set state to OFF, because it could be updating
    server = false;
  });
}

function deleteFolderRecursive(path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function(file, index){
      var curPath = path + "/" + file;
      if (fs.lstatSync(curPath).isDirectory()) {
        deleteFolderRecursive(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}