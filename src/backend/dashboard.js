const express = require('express');
const app = express();
const http = require('http').Server(app);
const fs = require('fs');
const port = process.env.PORT || 3000;
var io = require('socket.io')(http);
eval(fs.readFileSync(__dirname + '/mod/md5.js') + '');

console.log('Server started in directory: ' + __dirname);
const dashboardDirTree = __dirname.split('/');
const dashboardDir = dashboardDirTree[dashboardDirTree.length-2];

const LOGGER = require('./mod/backend-logger');
const frontendlogger = require('./mod/frontend-logger');
const configBroadcast = require('./mod/config-broadcast');
const languageBroadcast = require('./mod/language-broadcast');
const system = require('./mod/system');
const google = require('./mod/google');
const trello = require('./mod/trello');
//  Temporarily disabled because it requires administrator rights
//const wireless = require('./mod/wireless');

app.use(express.static(__dirname + '/../frontend'));

var frontendConnected = false;
var config = {};

function onConnection(socket) {
  LOGGER.log('INFO', 'Client connected to socket');
  new frontendlogger(LOGGER, socket);
  socket.on('request-lang', sendLanguage);
  socket.on('request-config', startModules);
  if (!frontendConnected) {
    refreshFrontend();
    loadConfig();
  }
  frontendConnected = true;
}

io.on('connection', onConnection);

http.listen(port, () => LOGGER.log('INFO', 'Listening on port ' + port));

LOGGER.log('INFO', 'Starting Dashboard...');

function refreshFrontend() {
  LOGGER.log('INFO', 'Sending refresh command to frontend');
  io.emit('refresh', true);
}

function loadConfig() {
  // Load config
  var configFile = __dirname + '/../../../config/config.json';
  LOGGER.log('INFO', 'Reading config: ' + configFile)
  fs.readFile(configFile, function processClientSecrets(err, content) {
    if (err) {
      LOGGER.log('ERROR', 'Error loading config: ' + err);
      return;
    }

    config = JSON.parse(content);

    if(!validateLicense(config)) {
      LOGGER.log('ERROR', 'Invalid License.');
      io = {};
      config = {};
    } else {
      LOGGER.log('INFO', 'License is valid.');
    }
  });
}

function sendLanguage() {
  if (config !== {}) {
    LOGGER.log('INFO', 'Sending Language...');

    new languageBroadcast(config, LOGGER, io);
  } else {
    LOGGER.log('WARN', 'Waiting until config is loaded...');
    setTimeout( function() {
      startModules();
    }, 1000);
  }
}

function startModules() {
  if (config !== {}) {
    LOGGER.log('INFO', 'Starting Modules...');

    new configBroadcast(config, LOGGER, io);
    new system(LOGGER);
    new google(config, LOGGER, io, dashboardDir);
    new trello(config, LOGGER, io);
    //  Temporarily disabled because it requires administrator rights
    //  new wireless(config, LOGGER, io);
  } else {
    LOGGER.log('WARN', 'Waiting until config is loaded...');
	setTimeout( function() {
      startModules();
    }, 1000);
  }
}

function validateLicense(config) {
  // Load licenses
  var content = JSON.parse(fs.readFileSync(__dirname + '/../../config/license_hash.json'));

  //UID
  var hashedUid = hex_md5(config.uid);

  //Licenses from license_hash.json
  for (var i = 0; i < content.licenses.length; i++) {
    if(content.licenses[i] === hashedUid) {
      return true;
    }
  }

  return false;
}