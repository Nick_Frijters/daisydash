const fs = require('fs');
const path = require('path');
const packageFolder = '/../../';
const languageFolder = '/../../../config';

var CONFIG, LOGGER, io

module.exports = function(config, logger, ioConfig, rootDir) {
  CONFIG = config;
  LOGGER = logger;
  io = ioConfig;
  LOGGER.log('INFO', 'Starting Language Broadcast');

  broadcastLanguage();
}

function broadcastLanguage() {
  LOGGER.log('INFO', 'Configured language: "' + CONFIG.locale.language + '"');

  var langInfo = JSON.parse(fs.readFileSync(__dirname + languageFolder + '/lang.json'));

  var userLangInfo = getUserLangInfo(langInfo, CONFIG.locale.language);

  io.emit('update-lang',
    userLangInfo
  );
}

function getUserLangInfo(langInfo, lang) {
  var userLangInfo = {};

  for (var key in langInfo) {
    if (langInfo.hasOwnProperty(key)) {
      var val = Object.getOwnPropertyDescriptor(langInfo[key], lang).value;
      userLangInfo[key] = val;
    }
  }

  return userLangInfo;
}