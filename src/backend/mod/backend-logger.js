var fs = require('fs');

module.exports.log = function log(level, message) {
  fs.appendFileSync('logging.txt', new Date().toUTCString() + " - " + level + ": " + message + "\r\n");
}