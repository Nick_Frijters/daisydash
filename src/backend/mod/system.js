const os = require('os-utils');
const fs = require('fs');

var LOGGER;

module.exports = function system(logger) {
  LOGGER = logger;
  LOGGER.log('INFO', 'Starting Module: System');
}

setInterval(function() {
  os.cpuUsage(function(v) {
    var cpu = v;
    var mem = 1 - os.freememPercentage();
    if (cpu > 0.9 || mem > 0.9) {
      LOGGER.log('WARN', 'Up (sec): ' + Math.round(os.processUptime()) + ', CPU%: ' + Math.round(cpu * 100) + ', MEM%: ' + Math.round(mem * 100));
    }
  });
}, 300000);