const Wireless = require('wireless');
// https://www.npmjs.com/package/wireless

var CONFIG, LOGGER, io;
var wl;
var current = '';

module.exports = function wireless(config, logger, ioConfig) {
  CONFIG = config;
  LOGGER = logger;
  io = ioConfig;

  LOGGER.log('INFO', 'Starting Module: Wireless');

  wl = new Wireless({
    iface: 'wlp2s0',
    connectionSpyFrequency: 2,
    vanishThreshold: 2
  });

  wl.enable(function(err) {
    wl.start();
  });

  wl.on('former', function(address) {
    networkConnected(address);
  });

  wl.on('join', function(network) {
    networkConnected(network.address);
  });

  wl.on('leave', function(network) {
    networkDisconnected();
  });

  wl.on('signal', function(network) {
    if (network.address === current) {
      var quality = Math.floor(network.quality / 70 * 100);
      io.emit('update-wifi-ssid', network.ssid);
      io.emit('update-wifi-quality', quality);
    }
  });

  wl.on('error', function(err) {
    LOGGER.log('ERROR', 'Network error: ' + err);
  });
}

function networkConnected(address) {
  LOGGER.log('INFO', 'Connected(F) to network: ' + address);
  current = address;
  io.emit('update-wifi-quality', 100);
  io.emit('assistant-message', {type: 'technical_status', message: 'We have established a network connection!'});
}

function networkDisconnected() {
  LOGGER.log('WARNING', 'Network connection lost!');
  current = '';
  io.emit('update-wifi-quality', 0);
  io.emit('assistant-message', {type: 'technical_status', message: 'Our network connection is lost!'});
}