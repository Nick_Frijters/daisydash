var LOGGER;

module.exports = function frontendlogger(logger, socket) {
  LOGGER = logger;

  socket.on('frontend-logging', function(data) {
    LOGGER.log('INFO', 'FRONTEND: ' + data);
  });
}