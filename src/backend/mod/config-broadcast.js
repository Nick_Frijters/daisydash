const fs = require('fs');
const path = require('path');
const packageFolder = '/../../';
const imgSource = '/../../../../config/img';
const imgTarget = '/../../frontend/img';
const configFolder = '/../../../config';

var CONFIG, LOGGER, io, systemInfo, images, lastImages, ads;

module.exports = function(config, logger, ioConfig) {
  CONFIG = config;
  LOGGER = logger;
  io = ioConfig;
  LOGGER.log('INFO', 'Starting Config Broadcast');

  systemInfo = JSON.parse(fs.readFileSync(__dirname + packageFolder + 'package.json', 'utf8'));
  ads = JSON.parse(fs.readFileSync(__dirname + configFolder + '/ads.json'));

  prepareImages();
  broadcast();
}

function broadcast() {
  broadcastLayout();
  broadcastAssistant();
  broadcastAdvertisements();
  broadcastCounter();
  broadcastSystem();
  broadcastTraffic();
  broadcastWeather();
  broadcastImages();
}

function broadcastLayout() {
  io.emit('update-layout',
    CONFIG.layout
  );
}

function broadcastAssistant() {
  io.emit('update-assistant',
    CONFIG.assistant
  );
}

function broadcastAdvertisements() {
  io.emit('update-advertisements',
    ads
  );
}

function broadcastCounter() {
  io.emit('update-counter',
    CONFIG.counter
  );
}

function broadcastSystem() {
  io.emit('update-system', {
    description: systemInfo.description,
    version: systemInfo.version
  });
}

function broadcastWeather() {
  io.emit('update-weather', {
    "location": CONFIG.locale.city + "," + CONFIG.locale.country,
    "key": CONFIG.credentials.openweathermap.key
  });
}

function broadcastTraffic() {
  io.emit('init-traffic', {
    "home": CONFIG.locale.home
  });
}

function prepareImages() {
  var sourceFolder = __dirname + imgSource;
  var targetFolder = __dirname + imgTarget;
  images = fs.readdirSync(sourceFolder);
  lastImages = [];

  if (!fs.existsSync(targetFolder)) {
    fs.mkdirSync(targetFolder);
  } else {
    fs.readdirSync(targetFolder).forEach(function(file) {
      var currentFile = targetFolder + '/' + file;
      fs.unlinkSync(currentFile);
    });
  }

  images.forEach(function(file) {
    var sourceFile = sourceFolder + '/' + file;
    var targetFile = targetFolder + '/' + file;
    fs.writeFileSync(targetFile, fs.readFileSync(sourceFile));
  });
}

function broadcastImages() {
  var image;

  do {
    image = images[Math.floor(Math.random()*images.length)];
  } while (lastImages.indexOf(image) >= 0)

  lastImages.push(image);
  if (lastImages.length >= Math.floor(images.length / 2)) {
    lastImages.shift();
  }

  io.emit('update-image', image);

  setTimeout(function() {
    broadcastImages();
  }, 60000);
}