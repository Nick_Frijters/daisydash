const fs = require('fs');
const readline = require('readline');
const google = require('googleapis');
const googleAuth = require('google-auth-library');
var CONFIG, LOGGER, io, root;
var events = [];
var users = [];

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/calendar-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];

module.exports = function google(config, logger, ioConfig, rootDir) {
  CONFIG = config;
  LOGGER = logger;
  io = ioConfig;
  root = rootDir;

  LOGGER.log('INFO', 'Starting Module: Google');

  users = CONFIG.users;
  for (var i = 0; i < users.length; i++) {
    events.push([]);
  }

  updateCalendarEvents(CONFIG);
}

function updateCalendarEvents(config) {
    var googleCredentials = config.credentials.google;
    var clientSecretInfo = googleCredentials.client_secret;

    var clientSecret = clientSecretInfo.installed.client_secret;
    var clientId = clientSecretInfo.installed.client_id;
    var redirectUrl = clientSecretInfo.installed.redirect_uris[0];
    var auth = new googleAuth();

    if (googleCredentials.tokens_calendar.length == 0) {
      LOGGER.log('ERROR', 'No Google credentials found!');
    }
    else {
      for(var i = 0; i < googleCredentials.tokens_calendar.length; i++) {
        var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
        oauth2Client.credentials = googleCredentials.tokens_calendar[i];
        getEvents(i, oauth2Client);
      }
    }

	setTimeout( function() {
      updateCalendarEvents(config);
    }, 60000);
}

function getEvents(index, auth) {
  var calendar = google.calendar('v3');
  calendar.events.list({
    auth: auth,
    calendarId: 'primary',
    timeMin: (new Date()).toISOString(),
    maxResults: 50,
    singleEvents: true,
    orderBy: 'startTime'
  }, function(err, response) {
    if (err) {
      LOGGER.log('ERROR', 'The calendar API returned an error: ' + err);
      return;
    } else {
      saveEvents(index, response.items);
      broadcastEvents();
    }
  });
}

function saveEvents(index, data) {
  data = data.map(function(element) {
    var color = element.colorId ? element.colorId : getColor(element.organizer);
    return {
      'id': element.id,
      'summary': element.summary,
      'colorId': color,
      'start': element.start,
      'end': element.end,
      'updated': element.updated,
      'location': element.location
    };
  });

  if (events.length === 0) {
    events[index] = data;
  }

  var newEvents = data.map(function(element) {
    for(var i = 0; i < events[index].length; i++) {
      if (events[index][i].id === element.id) {
        return {};
      }
    }
    return element;
  }).filter(value => Object.keys(value).length !== 0);

  var updatedEvents = data.map(function(element) {
    for(var i = 0; i < events[index].length; i++) {
      if (events[index][i].id === element.id && events[index][i].updated !== element.updated) {
        io.emit('update-calendar-event', element);
        return element;
      }
    }
    return {};
  }).filter(value => Object.keys(value).length !== 0);

  var removedEvents = events[index].map(function(element) {
    for(var i = 0; i < data.length; i++) {
      if (data[i].id === element.id) {
        return null;
      }
    }
    return element.id;
  }).filter(value => value !== null);

//  console.log('Existing: ' + events[index].length);
//  console.log('New     : ' + newEvents.length);
//  console.log('Updated : ' + updatedEvents.length);
//  console.log('Removed : ' + removedEvents.length);

  events[index] = data;
}

function getColor(organizer) {
  for(var i = 0; i < users.length; i++) {
    if (users[i].calendar && users[i].email === organizer.email) {
      return users[i].colorId;
    }
  }
  return '0';
}

function broadcastEvents() {
  io.emit('update-calendar', events);
}
