const fs = require('fs');
var Trello = require("node-trello");

const BASECOLOR = '#BDBDBD';
var CONFIG, LOGGER, io, t;
var colors = {};

module.exports = function trello(config, logger, ioConfig) {
  CONFIG = config;
  LOGGER = logger;
  io = ioConfig;

  LOGGER.log('INFO', 'Starting Module: Trello');

  loadColors(CONFIG.users);
  var trelloCredentials = CONFIG.credentials.trello;
  t = new Trello(trelloCredentials.key, trelloCredentials.token);
  updateTrelloStatus();
}

function updateTrelloStatus() {
    t.get("/1/boards/6bwWIOQR/cards", function(err, data) {
        if (err) {
          LOGGER.log('ERROR', 'The Trello API returned an error: ' + err);
        } else {
          if (data) {
            var minimizedTasks = [];
            data.forEach(function (task) {
              minimizedTasks.push({
                name: task.name,
                due: task.due,
                color: getTaskColor(task.labels)
              });
            });
            io.emit('update-tasks', minimizedTasks);
          } else {
            LOGGER.log('WARN', 'The Trello API did not provide usable data: ' + data);
          }
        }
      });

	setTimeout( function() {
      updateTrelloStatus()
    }, 5000);
}

function loadColors(users) {
  users.forEach(function (user) {
    if (!!user.tasks) {
      colors[user.name.toLowerCase()] = user.colorHex;
    }
  });
}

function getTaskColor(labels) {
  if (labels.length > 0) {
    return getUserColor(labels[0].name.toLowerCase());
  } else {
    return BASECOLOR;
  }
}

function getUserColor(name) {
  if (colors[name]) {
    return colors[name];
  } else {
    return BASECOLOR;
  }
}